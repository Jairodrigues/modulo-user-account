import * as actions from '../../store/Service/actions';

describe('getServiceByEventId => fulfill ', () => {
	const fulfill = {
		payload: [
			{
				id: '1',
				type: 'manpower',
				value: 'Pizzaiollo',
				qtd: '1',
			},
		],
		error: false,
	};

	it('Buscar', async () => {
		const response = await actions.getServiceByEventId(1);
		expect(response).toEqual(fulfill);
	});
});
