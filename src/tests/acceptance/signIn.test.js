const puppeteer = require('puppeteer');

const appUrlBase = 'http://localhost:3001';
const routes = {
	public: {
		login: `${appUrlBase}/login`,
	},
};

let browser;
let page;

beforeAll(async () => {
	// launch browser
	browser = await puppeteer.launch({
		headless: true, // headless mode set to false so browser opens up with visual feedback
		slowMo: 250, // how slow actions should be
	});
	// creates a new page in the opened browser
	page = await browser.newPage();
});

describe('Login', () => {
	it(
		'Usuário logando no sistema com sucesso',
		async () => {
			await page.goto(routes.public.login);
			await page.waitForSelector('.form-horizontal');
			await page.click('input[name=email]');
			await page.type('input[name=email]', 'jairo@email.com');
			await page.click('input[name=password]');
			await page.type('input[id=password]', 'Juninho99');
			await page.click('button[type=submit]');
			await page.waitForSelector('div[class="header-container"]');
		},
		1600000
	);
	it(
		'Usuário logando no sistema com senha e email invalidos',
		async () => {
			await page.goto(routes.public.login);
			await page.waitForSelector('.form-horizontal');
			await page.click('input[name=email]');
			await page.type('input[name=email]', 'jairo@teste.com');
			await page.click('input[name=password]');
			await page.type('input[id=password]', 'Jairo123');
			await page.click('button[type=submit]');
			await page.waitForSelector('div[class="modal-header error"]');
		},
		1600000
	);
});

// This function occurs after the result of each tests, it closes the browser
afterAll(() => {
	browser.close();
});
