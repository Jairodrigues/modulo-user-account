import ServiceHelper from './ServiceHelper';
import { URL } from '../utils/urlJoin';

export default class EventService {
	static newEvent = async payload => await ServiceHelper.SendPost(`${URL}/event`, payload);
	static eventById = async id => await ServiceHelper.SendGet(`${URL}/event/${id}`);
	static updateEvent = async (id, payload) => await ServiceHelper.SendPatch(`${URL}/event/${id}`, payload);
}
