import ServiceHelper from './ServiceHelper';
import { URL } from '../utils/urlJoin';

export default class ManPowerService {
	static serviceByEventId = async id => await ServiceHelper.SendGet(`${URL}/service/${id}`);
	static updateService = async (id, data) => await ServiceHelper.SendPatch(`${URL}/service/${id}`, data);
	static getOptionsService = async type => await ServiceHelper.SendGet(`${URL}/service/optionsService/${type}`);
}
