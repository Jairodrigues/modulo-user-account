import ServiceHelper from './ServiceHelper';
import { URL } from '../utils/urlJoin';

export default class PriceService {
	static getPriceByEvenetId = async eventId => await ServiceHelper.SendGet(`${URL}/price/${eventId}`);
	static updateParcel = async (id, parcel) => await ServiceHelper.SendPatch(`${URL}/price/${id}`, parcel);
}
