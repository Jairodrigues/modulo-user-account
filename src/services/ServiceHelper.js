import axios from 'axios';

const doRequest = async (url, method, payload) => {
	const request = {
		method,
		url,
		data: payload,
		headers: {
			'Content-Type': 'application/json',
		},
	};

	const token = localStorage.getItem('access_token');

	if (token) {
		request.headers.Authorization = `Bearer ${token}`;
	}

	return await axios(request);
};

export default class ServiceHelper {
	static validateToken(response) {
		if (response.status === 401) {
			localStorage.clear();
			window.location.href = '/login';
		}
		return response;
	}

	static async SendGet(url) {
		try {
			return await doRequest(url, 'get', null);
		} catch (error) {
			throw this.validateToken(error.response);
		}
	}

	static async SendPost(url, payload) {
		try {
			return await doRequest(url, 'post', payload);
		} catch (error) {
			throw this.validateToken(error.response);
		}
	}

	static async SendPut(url, payload) {
		try {
			return await doRequest(url, 'put', payload);
		} catch (error) {
			throw this.validateToken(error.response);
		}
	}

	static async SendPatch(url, payload) {
		try {
			console.log(payload);
			return await doRequest(url, 'patch', payload);
		} catch (error) {
			throw this.validateToken(error.response);
		}
	}
}
