import ServiceHelper from './ServiceHelper';
import { URL } from '../utils/urlJoin';

export default class CustomerServices {
	static getCustomersReceived = async () => await ServiceHelper.SendGet(`${URL}/event/status/recebido`);
	static getCustumerById = async id => await ServiceHelper.SendGet(`${URL}/custumers/${id}`);
	static updateCustumer = async (id, data) => await ServiceHelper.SendPatch(`${URL}/custumers/${id}`, data);
}
