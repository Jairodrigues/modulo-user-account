import ServiceHelper from './ServiceHelper';
import { URL } from '../utils/urlJoin';

export default class MenuService {
	static getMenus = async () => await ServiceHelper.SendGet(`${URL}/menus`);

	static getMenuById = async menuId => await ServiceHelper.SendGet(`${URL}/menu/${menuId}`);

	static getOptionsMenu = async type => await ServiceHelper.SendGet(`${URL}/menu/options/${type}`);

	static updateMenu = async (id, menu) => await ServiceHelper.SendPatch(`${URL}/menu/${id}`, menu);
}
