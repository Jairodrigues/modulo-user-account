import ServiceHelper from './ServiceHelper';
import { URL } from '../utils/urlJoin';

export default class AccountService {
	static async signIn(payload) {
		return await ServiceHelper.SendPost(`${URL}/login`, payload);
	}
}
