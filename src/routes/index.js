import React from 'react';
import { Switch, Route} from 'react-router-dom';
import SignIn from '../pages/signIn/containers';
import Authorization from './authorization';
import PrivateRouter from './components/private-router';

const Router = () => {
    return(
        <Switch>
			<Route path="/login" exact component={SignIn} />         			  
			<Authorization path="/" component={PrivateRouter}/>
		</Switch>
    );
}

export default Router;