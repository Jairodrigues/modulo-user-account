import React from 'react';
import { Route, Switch } from 'react-router-dom';
import BudgetsReceived from '../../pages/received/container';
import Budget from '../../pages/budget/containers';

const PrivateRouter = () => {
	return (
		<Switch>
			<Route path="/received" component={BudgetsReceived} />
			<Route path="/budget/:id" component={Budget} />
		</Switch>
	);
};

export default PrivateRouter;
