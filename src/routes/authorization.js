import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { isLoggedIn } from '../store/SignIn/selectors';

const Authorization = ({ isAuthenticated, component: Component, ...otherProps }) => {

  return(
  
  <Route
    {...otherProps}
    render={props =>
      (isAuthenticated ? <Component {...props} /> : <Redirect to="/login" />)
    }
  />
)};

Authorization.propTypes = {
  component: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool.isRequired
};

const mapStateToProps = state => ({ 
  isAuthenticated: isLoggedIn(state)
 });

export default connect(mapStateToProps)(Authorization);

