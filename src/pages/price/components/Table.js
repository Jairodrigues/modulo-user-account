import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Edit from './Form';
import { DialogModal } from '../../../common';

const PriceTable = ({ parcel, onDelete, updatePrice }) => {
	return (
		<Paper>
		<Table>
			<TableHead>
				<TableRow>
					<TableCell align="right">Parcela</TableCell>
					<TableCell align="right">Data</TableCell>
					<TableCell align="right">Banco</TableCell>
					<TableCell align="right">Conta</TableCell>
					<TableCell align="right">Valor</TableCell>
					<TableCell align="right">Status</TableCell>
					<TableCell align="center">Opções</TableCell>
				</TableRow>
			</TableHead>
			<TableBody>
				{parcel.map(row => {
					return (
						<TableRow key={row.id}>
							<TableCell align="right">{row.id}</TableCell>
							<TableCell align="right">{row.date}</TableCell>
							<TableCell align="right">{row.bank}</TableCell>
							<TableCell align="right">{row.account}</TableCell>
							<TableCell align="right">{row.amount}</TableCell>
							<TableCell align="right">{row.status}</TableCell>

							<TableCell align="right">
								<DialogModal title="Alterar Parcela" buttonTitle="editar">
									<Edit onSubmit={updatePrice} initialValues={row} />
								</DialogModal>
								<Button
									color="secondary"
									size="large"
									variant="outlined"
									style={{ marginLeft: '5px' }}
									onClick={evt => onDelete(row)}
								>
									Excluir
								</Button>
							</TableCell>
						</TableRow>
					);
				})}
			</TableBody>
		</Table>
		</Paper>
	);
};

PriceTable.propTypes = {
	price: PropTypes.array.isRequired,
};

PriceTable.defaultProps = {
	price: [],
};

export default PriceTable;
