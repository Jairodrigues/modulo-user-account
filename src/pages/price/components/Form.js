import React from 'react';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import { TextField } from '../../../common/index';
import { Field, reduxForm } from 'redux-form';

const Edit = ({ handleSubmit, pristine, submitting, loading }) => {
	return (
		<form onSubmit={handleSubmit}>
			<Field name="id" component={TextField} label="Parcela" type="number" />
			<Field name="date" component={TextField} label="Data de vencimento" type="text" />
			<Field name="bank" component={TextField} label="Banco" type="text" />
			<Field name="account" component={TextField} label="Ag/Conta" type="text" />
			<Field name="amount" component={TextField} label="Valor" type="text" />
			<Field name="status" component={TextField} label="Status" type="text" />
			<DialogActions>
				<Button onClose type="submit" fullWidth variant="contained" color="primary" disabled={pristine || submitting}>
					{loading ? 'AGUARDE...' : 'ADICIONAR'}
				</Button>
			</DialogActions>
		</form>
	);
};
export default reduxForm({ form: 'form' })(Edit);
