import React, { Component, Fragment } from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import PriceTable from './components/Table';
import { DialogModal } from '../../common';
import CardActions from '@material-ui/core/CardActions';
import Edit from './components/Form';
import PriceService from '../../services/PriceService';

export default class Price extends Component {
	constructor() {
		super();
		this.state = {
			price: {},
			render: false,
		};
	}

	addPrice = async item => {
		const { _id } = this.state.price;
		try {
			this.state.price.parcel.push(item);
			const { data } = await PriceService.updateParcel(_id, this.state.price);
			this.setState(prevState => ({ ...prevState, price: data }));
		} catch (err) {
			console.log(err);
		}
	};

	updatePrice = async item => {
		console.log(item);
		const { _id, parcel } = this.state.price;
		var filtered = parcel.filter(value => {
			if (value.id !== item.id) return value;
		});
		this.state.price.parcel = filtered;
		this.state.price.parcel.push(item);
		try {
			const { data } = await PriceService.updateParcel(_id, this.state.price);
			this.setState(prevState => ({ ...prevState, price: data }));
		} catch (err) {
			console.log(err);
		}
	};

	deleteParcel = async item => {
		try {
			const { price } = this.state;
			var filtered = price.parcel.filter(value => {
				return value !== item;
			});
			price.parcel = filtered;
			const { data } = await PriceService.updateParcel(price._id, price);
			this.setState(prevState => ({ ...prevState, price: data, render: true }));
		} catch (err) {
			console.log(err);
		}
	};

	handlePrice = async eventId => {
		try {
			const { data } = await PriceService.getPriceByEvenetId(eventId);
			this.setState(prevState => ({ ...prevState, price: data[0], render: true }));
		} catch (err) {
			console.log(err);
		}
	};

	componentDidMount() {
		const { event } = this.props;
		if (event) this.handlePrice(event);
	}

	render() {
		const { price, render } = this.state;
		return (
			render && (
				<Fragment>
					<Card style={{ marginTop: '10px', padding: '20px' }}>
						<CardContent>
							<List>
								<Grid container spacing={24}>
									<Grid item xs={12} sm={12}>
										<PriceTable parcel={price.parcel} updatePrice={this.updatePrice} onDelete={this.deleteParcel} />
									</Grid>
								</Grid>
							</List>
						</CardContent>

						<CardActions style={{ padding: '20px' }}>
							<DialogModal title="Adicionar parcela" buttonTitle="NOVA PARCELA">
								<Edit onSubmit={this.addPrice} />
							</DialogModal>
						</CardActions>
					</Card>
				</Fragment>
			)
		);
	}
}
