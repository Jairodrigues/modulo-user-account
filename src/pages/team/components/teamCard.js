import React from 'react';
import PropTypes from 'prop-types';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';

import { DialogModal } from '../../../common';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';

const TeamCard = () => (
	<Card style={{ marginTop: '10px', padding: '20px' }}>
		<CardHeader title="Equipe" />
		<CardContent>
			<Typography variant="body1">
				<b>Nome:</b>
				<br />
				<b>Telefone:</b>
				<br />
				<b>Horario de chegada:</b>
				<br />
			</Typography>
		</CardContent>
		<CardActions>
			<DialogModal title="Editar dados do Cliente" />
		</CardActions>
	</Card>
);

TeamCard.propTypes = {};

TeamCard.defaultProps = {};

export default TeamCard;
