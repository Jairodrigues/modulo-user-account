import React, { Component } from 'react';
import Section from './components/sections';
import Option from './components/options';
import PeopleIcon from '@material-ui/icons/People';
import ManPowerService from '../../services/ManProwerService';
import CardContent from '@material-ui/core/CardContent';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';

class Service extends Component {
	constructor(props) {
		super(props);
		this.state = {
			render: false,
			data: {},
			options: [{}],
		};
	}

	handleUpdateService = async (item, type) => {
		try {
			const { _id } = this.state.data;
			this.state.data[type].push(item);
			const { data } = await ManPowerService.updateService(_id, this.state.data);
			this.setState(prevState => ({ ...prevState, data: data }));
		} catch (err) {
			console.log('Modal de erro');
		}
	};

	handleDelete = async (item, type) => {
		try {
			const { _id } = this.state.data;
			this.state.data[type].pop(item);
			const { data } = await ManPowerService.updateService(_id, this.state.data);
			this.setState(prevState => ({ ...prevState, data: data }));
		} catch (err) {
			console.log('Modal de erro');
		}
	};

	handleOptionServices = async type => {
		try {
			const { data } = await ManPowerService.getOptionsService(type);
			this.setState(prevState => ({ ...prevState, options: data, render: true }));
		} catch (err) {
			console.log('Modal de erro');
		}
	};

	handleServices = async eventId => {
		try {
			const { data } = await ManPowerService.serviceByEventId(eventId);
			this.setState(prevState => ({ ...prevState, data: data[0], render: true }));
		} catch (err) {
			console.log('Modal de erro');
		}
	};

	componentDidMount = () => {
		const { event } = this.props;
		if (event) this.handleServices(event);
	};

	render() {
		const { render, options } = this.state;
		const { AdditionalProduct, AdditionalService, manpower, utensils } = this.state.data;
		return (
			render && (
				<Card style={{ marginTop: '10px', padding: '20px' }}>
					<CardHeader title="Serviços" />
					<CardContent>
						<Section
							iconSection={<PeopleIcon />}
							sectionName="Mão de obra"
							type="manpower"
							deleteOption={this.handleDelete}
							data={manpower}
						>
							<Option
								handleListItemClick={this.handleUpdateService}
								options={this.handleOptionServices}
								data={options}
								type="manpower"
								title="Mão de Obra"
							/>
						</Section>
						<Section
							iconSection={<PeopleIcon />}
							sectionName="Utensilios"
							type="utensils"
							deleteOption={this.handleDelete}
							data={utensils}
						>
							<Option
								handleListItemClick={this.handleUpdateService}
								options={this.handleOptionServices}
								data={options}
								type="utensils"
								title="Utensilios"
							/>
						</Section>
						<Section
							iconSection={<PeopleIcon />}
							sectionName="Serviços adicionais"
							type="AdditionalService"
							deleteOption={this.handleDelete}
							data={AdditionalService}
						>
							<Option
								handleListItemClick={this.handleUpdateService}
								options={this.handleOptionServices}
								data={options}
								type="AdditionalService"
								title="Serviços adicionais"
							/>
						</Section>
						<Section
							iconSection={<PeopleIcon />}
							sectionName="Produtos adicionais"
							type="AdditionalProduct"
							deleteOption={this.handleDelete}
							data={AdditionalProduct}
						>
							<Option
								handleListItemClick={this.handleUpdateService}
								options={this.handleOptionServices}
								data={options}
								type="AdditionalProduct"
								title="Produtos adicionais"
							/>
						</Section>
					</CardContent>
				</Card>
			)
		);
	}
}

export default Service;
