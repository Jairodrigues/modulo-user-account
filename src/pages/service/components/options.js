import React, { useState } from 'react';
import PropTypes from 'prop-types';

import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import AddIcon from '@material-ui/icons/Add';
import List from '@material-ui/core/List';
import Fab from '@material-ui/core/Fab';

import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';

const Options = ({ data, title, type, options, handleListItemClick }) => {
	const [state, setState] = useState({ open: false });

	function handleClose() {
		setState({ ...state, open: false });
	}
	function handleClickOpen() {
		options(type);
		setState({ ...state, open: true, type: 'entrada' });
	}

	return (
		<div>
			<Fab color="primary" style={{ width: '40px', height: '40px' }} aria-label="Add" onClick={handleClickOpen}>
				<AddIcon />
			</Fab>
			<Dialog open={state.open} onClose={handleClose} aria-labelledby="form-dialog-title">
				<DialogTitle id="form-dialog-title">{title}</DialogTitle>
				<List>
					{data.map(item => (
						<ListItem button key={item.id + item.value} onClick={e => handleListItemClick(item, type, e)}>
							<ListItemAvatar>
								<Avatar>
									<AddIcon />
								</Avatar>
							</ListItemAvatar>

							<ListItemText primary={item.qtd + ' ' + item.value} />
						</ListItem>
					))}
				</List>
			</Dialog>
		</div>
	);
};

Options.propTypes = {
	title: PropTypes.string.isRequired,
	data: PropTypes.array.isRequired,
};

Options.defaultProps = {
	title: 'Adicionar opção',
	data: [],
};

export default Options;
