import React from 'react';
import Chip from '@material-ui/core/Chip';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import Divider from '@material-ui/core/Divider';
import PropTypes from 'prop-types';

const Section = ({ children, iconSection, sectionName, deleteOption, data, type }) => {
	return (
		<List>
			<ListItem>
				<Avatar>{iconSection}</Avatar>
				<ListItemText primary={sectionName} style={{ flex: '0 auto' }} />
				{data.map(item => {
					return (
						<Chip
							key={item.id + item.value}
							style={{ marginRight: '10px' }}
							onDelete={e => deleteOption(item, type, e)}
							label={item.qtd + ' ' + item.value}
						/>
					);
				})}
				{children}
			</ListItem>
			<Divider variant="inset" />
		</List>
	);
};

Section.propTypes = {
	children: PropTypes.element.isRequired,
	iconSection: PropTypes.element.isRequired,
	sectionName: PropTypes.string.isRequired,
	deleteOption: PropTypes.func.isRequired,
};

export default Section;
