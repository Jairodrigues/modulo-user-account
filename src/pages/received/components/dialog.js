import React, { Fragment } from 'react';
import Button from '@material-ui/core/Button';
import { useState } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';

function Modal({ children }) {
	const [state, setState] = useState({
		age: '',
		open: false,
	});

	function handleClickOpen() {
		setState({ ...state, open: true });
	}

	function handleClose() {
		setState({ ...state, open: false });
	}

	return (
		<Fragment>
			<Button color="inherit" size="large" variant="outlined" onClick={handleClickOpen}>
				Novo Orçameto
			</Button>

			<Dialog open={state.open} onClose={handleClose} aria-labelledby="form-dialog-title">
				<DialogTitle id="form-dialog-title">Adicionar Orçamento</DialogTitle>
				{children}
			</Dialog>
		</Fragment>
	);
}

export default Modal;
