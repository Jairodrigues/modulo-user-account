import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import CameraIcon from '@material-ui/icons/PhotoCamera';
import { styles } from './styles';

const Header = ({ classes }) => (
	<div className={classes.root}>
		<AppBar position="static">
			<Toolbar>
				<CameraIcon className={classes.icon} />
				<Button color="inherit" variant="outlined">
					Orçamentos Recebidos
				</Button>
			</Toolbar>
		</AppBar>
	</div>
);

Header.propTypes = {
	classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Header);
