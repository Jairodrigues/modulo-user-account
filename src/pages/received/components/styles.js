export const styles = theme => ({
	root: {
		flexGrow: 1,
		color: theme.palette.primary.main,
	},
	Title: {
		paddingTop: '50px',
	},
	icon: {
		marginRight: theme.spacing.unit * 2,
	},
});
