import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import { styles } from './styles';

const TableData = ({ classes, rows }) => (
	<Fragment>
		<Typography className={classes.Title} variant="subtitle1" gutterBottom>
			Listagem de orçamentos recebidos
		</Typography>
		<Paper className={classes.root}>
			<Table className={classes.table}>
				<TableHead>
					<TableRow>
						<TableCell>Data</TableCell>
						<TableCell align="right">Horario</TableCell>
						<TableCell align="right">Nome</TableCell>
						<TableCell align="right">Bairro</TableCell>
						<TableCell align="right">Convidados</TableCell>
						<TableCell align="right">Detalhes</TableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					{rows.map(row => {
						return (
							<TableRow key={row._id}>
								<TableCell component="th" scope="row">
									{row.date}
								</TableCell>
								<TableCell align="right">{row.eventSchedule}</TableCell>
								<TableCell align="right">{row.location}</TableCell>
								<TableCell align="right">{row.location}</TableCell>
								<TableCell align="right">{row.guests}</TableCell>
								<TableCell align="right">
									<Button color="inherit" component={Link} to={`/budget/${row._id}`} variant="outlined">
										DETALHES
									</Button>
								</TableCell>
							</TableRow>
						);
					})}
				</TableBody>
			</Table>
		</Paper>
	</Fragment>
);

TableData.propTypes = {
	classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TableData);
