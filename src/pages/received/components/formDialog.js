import React from 'react';

import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import SelectField from '../../../common/Form/selectField';
import TextField from '../../../common/Form/textField';

import { Field, reduxForm } from 'redux-form';
import { useState } from 'react';

function FormDialog({ handleSubmit, pristine, submitting, loading, menuItems }) {
	const [state, setState] = useState({
		evento: '',
	});

	function handleChange(event) {
		setState({
			...state,
			[event.target.name]: event.target.value,
		});
	}

	return (
		<div>
			<DialogContent>
				<form onSubmit={handleSubmit}>
					<Field name="email" component={TextField} autoFocus label="Email" type="email" />
					<Field name="telefone" component={TextField} label="Telefone" type="number" />
					<Field name="endereco" component={TextField} label="Endereço" type="text" />
					<Field name="bairro" component={TextField} label="Bairro" type="text" />
					<Field name="cep" component={TextField} label="CEP" type="number" />
					<Field name="qtd" component={TextField} label="Quantidade de Convidados" type="number" />
					<Field
						name="evento"
						items={menuItems}
						component={SelectField}
						handleChange={handleChange}
						id="evento"
						chosenValue={state.evento}
						label="Tipo de Envento"
					/>

					<DialogActions>
						<Button type="submit" fullWidth variant="contained" color="primary" disabled={pristine || submitting}>
							{loading ? 'AGUARDE...' : 'ADICIONAR'}
						</Button>
					</DialogActions>
				</form>
			</DialogContent>
		</div>
	);
}

export default reduxForm({ form: 'form' })(FormDialog);
