import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

import { styles } from './styles';

const Title = ({ classes }) => (
	<Fragment>
		<Typography
			className={classes.Title}
			component="h1"
			variant="h2"
			align="center"
			color="textPrimary"
			gutterBottom
		>
			Orçamentos Recebidos
		</Typography>
	</Fragment>
);

Title.propTypes = {
	classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Title);
