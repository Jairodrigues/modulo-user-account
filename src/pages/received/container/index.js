import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

//import * as CustumerActions from '../../../store/customer/actions';
import * as EventActions from '../../../store/events/actions';
import * as MenuActions from '../../../store/menu/actions';

import Alert from '../../../common/Modal/Alert';
import Header from '../components/header';
import TableData from '../components/table';
import Title from '../components/Title';
import Modal from '../components/dialog';
import Grid from '@material-ui/core/Grid';
import FormDialog from '../components/formDialog';

class BudgetsReceived extends Component {
	constructor(props) {
		super(props);
		this.state = {
			menuItems: [],
			custumers: [],
			AlertOpen: false,
			AlertMessage: null,
			error: false,
		};
	}

	componentDidMount = async () => {
		const { menu, getCustomersReceived } = this.props;
		const menus = await menu();
		const custumersList = await getCustomersReceived();
		console.log(custumersList);
		this.setState(prevState => ({ ...prevState, menuItems: menus.payload, custumers: custumersList.payload }));
	};

	submit = async data => {
		const { add } = this.props;
		const { message, error } = await add(data);
		if (!error) {
			this.setState(prevState => ({
				...prevState,
				AlertOpen: true,
				AlertMessage: message,
			}));
		} else {
			this.setState(prevState => ({
				...prevState,
				AlertOpen: true,
				AlertMessage: message,
				error: true,
			}));
		}
	};

	handleClose = () => {
		this.setState(prevState => ({ ...prevState, AlertOpen: false }));
	};

	render() {
		const { menuItems, custumers, AlertOpen, AlertMessage, error } = this.state;
		return (
			<Fragment>
				<Alert open={AlertOpen} close={this.handleClose} erro={error} title={error ? 'Ops...' : 'Boa...'} message={AlertMessage} />
				<Header />
				<Grid style={{ margin: '30px' }}>
					<Title />
					<Grid container justify="flex-end">
						<Modal>
							<FormDialog menuItems={menuItems} onSubmit={this.submit} />
						</Modal>
					</Grid>
					<TableData rows={custumers} />
				</Grid>
			</Fragment>
		);
	}
}

const mapDispatchToProps = dispatch =>
	bindActionCreators(
		{
			...MenuActions,
			//...CustumerActions,
			...EventActions,
		},
		dispatch
	);
export default connect(
	null,
	mapDispatchToProps
)(BudgetsReceived);
