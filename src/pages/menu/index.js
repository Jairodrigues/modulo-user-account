import React, { Component, Fragment } from 'react';
//service
import MenuService from '../../services/MenuService';
//components
import Section from './components/menuSections';
import Options from './components/menuOptions';
//icons
import AddIcon from '@material-ui/icons/Add';
import ScheduleIcon from '@material-ui/icons/Schedule';
import LocalBarIcon from '@material-ui/icons/LocalBar';
import CakeIcon from '@material-ui/icons/Cake';
import RestaurantMenu from '@material-ui/icons/RestaurantMenu';
//commom
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';

class Menu extends Component {
	constructor() {
		super();
		this.state = {
			data: {},
			menuOptions: [{}],
			render: false,
		};
	}

	handleDelete = async (option, type) => {
		try {
			const { data } = this.state;
			if (type === 'drink') {
				console.log(data.drinks);
				data.drinks.splice(data.drinks.indexOf(option), 1);
			} else {
				delete data[type][option];
			}
			const response = await MenuService.updateMenu(data._id, data);
			this.setState(prevState => ({ ...prevState, data: response.data }));
		} catch (err) {
			console.log('Modal de erro');
		}
	};

	handleMenu = async id => {
		try {
			const { data } = await MenuService.getMenuById(id);
			console.log('DATA', data);
			this.setState(prevState => ({ ...prevState, data: data[0], render: true }));
		} catch (err) {
			console.log('Modal de erro');
		}
	};

	handleMenuOptions = async type => {
		try {
			const { data } = await MenuService.getOptionsMenu(type);
			this.setState(prevState => ({ ...prevState, menuOptions: data }));
		} catch (err) {
			console.log('Modal de erro');
		}
	};

	handleUpdateMenu = async (option, type) => {
		const { data } = this.state;
		const { name, resume } = option;
		data[type][name] = resume;
		try {
			const response = await MenuService.updateMenu(data._id, data);
			this.setState(prevState => ({ ...prevState, data: response.data }));
		} catch (err) {
			console.log('Modal de erro');
		}
	};

	componentDidMount = async () => {
		const { event } = this.props;
		await this.handleMenu(event);
	};

	render() {
		const { appetizer, dessert, drinks, additional, main } = this.state.data;
		const { menuOptions } = this.state;

		return (
			<Fragment>
				{this.state.render && (
					<Card style={{ marginTop: '10px', padding: '20px' }}>
						<CardHeader title="Cardápio" />
						{console.log(this.state.data)}

						<CardContent>
							<Section
								iconSection={<ScheduleIcon />}
								sectionName="Entradas"
								type="appetizer"
								data={appetizer}
								deleteOption={this.handleDelete}
							>
								<Options
									data={menuOptions}
									options={this.handleMenuOptions}
									type="appetizer"
									title="Entradas"
									updateMenu={this.handleUpdateMenu}
								/>
							</Section>

							<Section
								iconSection={<RestaurantMenu />}
								sectionName="Prato Principal"
								data={main}
								type="main"
								deleteOption={this.handleDelete}
							>
								<Options
									data={menuOptions}
									options={this.handleMenuOptions}
									title="Prato Prinicipal"
									type="main"
									updateMenu={this.handleUpdateMenu}
								/>
							</Section>
							<Section
								iconSection={<LocalBarIcon />}
								sectionName="Bebidas"
								data={drinks}
								type="drink"
								deleteOption={this.handleDelete}
							>
								<Options
									data={menuOptions}
									options={this.handleMenuOptions}
									title="Bebidas"
									type="drink"
									updateMenu={this.handleUpdateMenu}
								/>
							</Section>

							<Section
								iconSection={<CakeIcon />}
								sectionName="Sobremesas"
								data={dessert}
								type="dessert"
								deleteOption={this.handleDelete}
							>
								<Options
									data={menuOptions}
									options={this.handleMenuOptions}
									type="dessert"
									title="Sobremesas"
									updateMenu={this.handleUpdateMenu}
								/>
							</Section>

							<Section
								iconSection={<AddIcon />}
								sectionName="Opções Adicionais"
								type="additional"
								data={additional}
								deleteOption={this.handleDelete}
							>
								<Options
									data={menuOptions}
									options={this.handleMenuOptions}
									type="additional"
									title="Adicionais"
									updateMenu={this.handleUpdateMenu}
								/>
							</Section>
						</CardContent>
					</Card>
				)}
			</Fragment>
		);
	}
}

export default Menu;
