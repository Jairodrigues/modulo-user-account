import React from 'react';
import Chip from '@material-ui/core/Chip';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import Divider from '@material-ui/core/Divider';

function convertArrayToObject(data) {
	if (Array.isArray(data)) {
		let newObject = Object.assign({}, data);
		return Object.values(newObject);
	}
	return Object.keys(data);
}

const Section = ({ children, iconSection, sectionName, deleteOption, data, type }) => {
	return (
		<List>
			<ListItem>
				<Avatar>{iconSection}</Avatar>
				<ListItemText primary={sectionName} style={{ flex: '0 auto' }} />
				{convertArrayToObject(data).map(item => {
					return <Chip key={item} style={{ marginRight: '10px' }} onDelete={() => deleteOption(item, type)} label={item} />;
				})}
				{children}
			</ListItem>
			<Divider variant="inset" />
		</List>
	);
};

Section.propTypes = {};

Section.defaultProps = {};

export default Section;
