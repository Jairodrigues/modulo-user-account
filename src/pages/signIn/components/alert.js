import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import withStyles from '@material-ui/core/styles/withStyles';
import DialogTitle from '@material-ui/core/DialogTitle';
import Grid from '@material-ui/core/Grid';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import PropTypes from 'prop-types';

import { styles } from './styles';

const Alert = ({ close, open, classes, message, icon, title }) => (
	<Dialog aria-labelledby="simple-dialog-title" open={open} onClose={close}>
		<Grid container direction="column" justify="center" alignItems="center">
			<i className={`material-icons ${classes.alertIcon}`}>{icon}</i>
			<DialogTitle className={classes.alertTitle}>{title}</DialogTitle>
			<DialogContent>
				<DialogContentText>{message}</DialogContentText>
			</DialogContent>
		</Grid>
	</Dialog>
);

Alert.propTypes = {
	classes: PropTypes.object.isRequired,
	onClose: PropTypes.func,
	selectedValue: PropTypes.string,
};

export default withStyles(styles)(Alert);
