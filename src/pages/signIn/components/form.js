import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { required, passwordValidate, email } from '../../../utils/validations';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import withStyles from '@material-ui/core/styles/withStyles';
import { styles } from './styles';
import Button from '@material-ui/core/Button';

const renderField = ({ input, type, label, meta: { touched, error } }) => {
	let isValid = false;
	if (touched && error) isValid = true;
	return (
		<FormControl margin="normal" error={isValid} required fullWidth>
			<InputLabel>{label}</InputLabel>
			<Input {...input} placeholder={label} type={type} />
			<FormHelperText id="component-error-text">{isValid && <span>{error}</span>}</FormHelperText>
		</FormControl>
	);
};

const Form = ({ handleSubmit, pristine, submitting, loading, classes, touched, error }) => {
	return (
		<form onSubmit={handleSubmit} className={classes.form}>
			<Field
				name="email"
				component={renderField}
				autoComplete="email"
				autoFocus
				label="Email"
				type="email"
				error={touched ? error : ''}
				validate={[required, email]}
			/>

			<Field
				name="password"
				component={renderField}
				autoComplete="current-password"
				type="password"
				label="Password"
				error={touched ? error : ''}
				validate={[required, passwordValidate]}
			/>

			<Button
				type="submit"
				fullWidth
				variant="contained"
				color="primary"
				disabled={pristine || submitting}
				className={classes.submit}
			>
				{loading ? 'AGUARDE...' : 'ENTRAR'}
			</Button>
		</form>
	);
};

export default reduxForm({ form: 'form' })(withStyles(styles)(Form));
