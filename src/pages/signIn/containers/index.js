import React, { Component, Fragment } from 'react';
import CardLogin from '../components/card';
import { connect } from 'react-redux';
import * as SigInActions from '../../../store/SignIn/actions';
import { bindActionCreators } from 'redux';
import Form from '../components/form';
import Alert from '../components/alert';

class SignIn extends Component {
	constructor(props) {
		super(props);
		this.state = {
			open: false,
			error: null,
		};
	}

	handleClose = () => {
		this.setState({ open: false });
	};

	submit = async data => {
		const { login, history } = this.props;
		const { error, message } = await login(data);
		if (!error) {
			history.push('/received');
		} else {
			this.setState({ open: true, error: message });
		}
	};

	render() {
		const { open, error } = this.state;
		return (
			<Fragment>
				<Alert open={open} close={this.handleClose} icon={'cancel'} title={'Ops..'} message={error} />;
				<CardLogin>
					<Form onSubmit={this.submit} />
				</CardLogin>
			</Fragment>
		);
	}
}

const mapDispatchToProps = dispatch => bindActionCreators({ ...SigInActions }, dispatch);

export default connect(
	null,
	mapDispatchToProps
)(SignIn);
