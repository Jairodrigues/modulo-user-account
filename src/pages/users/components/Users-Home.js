import React, { Fragment } from 'react';
import SearchInput from 'react-search-input';
import './styles.css';

const Actions = () => (
	<div className="actions">
		<div className="icon-edit-button" />
		<div className="icon-trash-button" />
	</div>
);

const SearchTable = onChangeSearch => (
	<SearchInput
		class="search-input"
		type="text"
		placeholder="Pesquisar por nome, perfil e status"
		onChange={onChangeSearch}
	/>
);

const List = ({ onChangeSearch, usersSearch }) => (
	<Fragment>
		<div className="table-header">{SearchTable(onChangeSearch)}</div>

		<table className="table">
			<thead>
				<tr className="header-table-charge">
					<th />
					<th className="title-table-charge" scope="col">
						1NOME
					</th>
					<th className="title-table-charge" scope="col">
						PERFIL DO USUÁRIO
					</th>
					<th className="title-table-charge" scope="col">
						STATUS
					</th>
					<th className="title-table-charge" scope="col">
						AÇÕES
					</th>
				</tr>
			</thead>
			{usersSearch.map(item => (
				<tbody key={item.name + item.id}>
					<tr>
						<td />
						<td>
							<span className="badge">XI</span>
							<b>{item.name}</b>
						</td>
						<td>
							<b>{item.role}</b>
						</td>
						<td>
							<b>{item.status}</b>
						</td>
						<td>{Actions()}</td>
					</tr>
				</tbody>
			))}
		</table>
	</Fragment>
);

export default List;
