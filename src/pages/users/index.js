import React from 'react';
import { Switch, Route } from 'react-router-dom';
import ListUser from './containers/users-home';

const Users = () => (
  <Switch>
        <Route path="/users" component={ListUser} />
  </Switch>
);

export default Users;
