import React, { Component, Fragment } from 'react';
import List from '../components/Users-Home';
import AccountService from '../../../services/AccountService';
import { createFilter } from 'react-search-input';

const KEYS_TO_FILTERS = ['name', 'status', 'role'];

class ListUsers extends Component {
	constructor() {
		super();
		this.state = {
			usersAll: [],
			usersPaginated: [],
			page: 0,
			limit: 3,
			searchTerm: '',
			UsersCount: 0,
		};
	}

	getUsersPaginated = async () => {
		const { page, limit } = this.state;
		const response = await AccountService.getUsersPaginated(page, limit);
		this.setState({ usersPaginated: response.data });
	};

	getUsersAll = async () => {
		const response = await AccountService.getUsersAll();
		this.setState({ usersAll: response.data, UsersCount: response.data.length });
	};

	componentDidMount() {
		this.getUsersPaginated();
		this.getUsersAll();
	}

	searchUpdated(term) {
		this.getUsersAll();
		this.setState({ searchTerm: term });
	}

	handlePageChange = async pageRequested => {
		pageRequested = pageRequested + 1;
		await this.setState({ page: pageRequested });
		this.getUsersPaginated();
	};

	showPagination = () => {
		//const { UsersCount, limit } = this.state;
		//const pageCount = UsersCount / limit;
		return ''; //<Pagination pageCount={pageCount} onPageChange={this.handlePageChange} />;
	};

	render() {
		const { searchTerm, usersAll, usersPaginated } = this.state;
		const filteredUsers = usersAll.filter(createFilter(searchTerm, KEYS_TO_FILTERS));
		return (
			<Fragment>
				<List {...this.state} usersSearch={searchTerm === '' ? usersPaginated : filteredUsers} onChangeSearch={this.searchUpdated} />
				<div className="mt-5">{searchTerm === '' ? this.showPagination() : ''}</div>
			</Fragment>
		);
	}
}

export default ListUsers;
