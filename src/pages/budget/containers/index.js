import React, { Component, Fragment } from 'react';
import Grid from '@material-ui/core/Grid';
import { Title, Header, CenteredTabs } from '../../../common';
import { DialogModal } from '../../../common';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Button from '@material-ui/core/Button';
import Custumers from '../../custumers';
import Avatar from '@material-ui/core/Avatar';
import Event from '../../event/index';
import Menu from '../../menu/index';
import Service from '../../service';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Price from '../../price';
import * as EventActions from '../../../store/events/actions';
import * as PriceActions from '../../../store/Price/actions';
import BeachAccessIcon from '@material-ui/icons/BeachAccess';
import Edit from '../components/editStatus'

class Budget extends Component {
	constructor(props) {
		super(props);
		this.state = { event: null, user: {}, price: {} };
	}

	getEventById = async () => {
		const { getEventById, match } = this.props;
		const { error, payload } = await getEventById(match.params.id);

		if (error) {
			console.log('Evento não encontrado');
		} else {
			this.setState(prevState => ({ ...prevState, event: payload }));
		}
	};

	componentDidMount = async () => {
		await this.getEventById();
	};

	render() {
		const { event } = this.state;
		return (
			<Fragment>
				<Header />
				
				{event ? (
					<Grid style={{ margin: '30px' }}>
						<Title text={`Pizzaiollo em Casa`}/>
						<ListItem>
							<Avatar>
          						<BeachAccessIcon />
        					</Avatar>
							<ListItemText primary="Status" secondary={event.status} />
							<Avatar>
          						<BeachAccessIcon />
        					</Avatar>
						 	<ListItemText primary="Data do Evento" secondary={event.date} />
							 <Avatar>
          						<BeachAccessIcon />
        					</Avatar>
						 	<ListItemText primary="Data de Solicitação" secondary={event.createdAt} />
							 <Avatar>
          						<BeachAccessIcon />
        					</Avatar>
						 	<ListItemText primary="Quantidade de Convidados" secondary={event.guests} />
							 <Avatar>
          						<BeachAccessIcon />
        					</Avatar>
						 	<ListItemText primary="Valor" secondary={event.amount} />
      					</ListItem>
						  <br/>
						<Grid container direction="row" style={{ marginBottom: '10px' }} justify="flex-end">
						<DialogModal title="Alterar status" buttonTitle="Alterar Status">
						<Edit/>
			
						</DialogModal>
							<Button variant="contained" color="primary" style={{ marginRight: '10px',marginLeft: '10px' }}>
								Recibo
							</Button>
							<Button variant="contained" color="primary">
								Contrato
							</Button>
						</Grid>
						<CenteredTabs
							user={<Custumers userId={event.user_id} />}
							event={<Event event={event} />}
							menu={<Menu event={event._id} />}
							service={<Service event={event._id} />}
							price={<Price event={event._id} />}
						/>
					</Grid>
				) : (
					''
				)}
			</Fragment>
		);
	}
}

const mapDispatchToProps = dispatch =>
	bindActionCreators(
		{
			...EventActions,
			//...CustumersAction,
			...PriceActions,
		},
		dispatch
	);
export default connect(
	null,
	mapDispatchToProps
)(Budget);
