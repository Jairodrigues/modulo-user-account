import React from 'react';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import SelectField from '../../../common/Form/selectField';
import { Field, reduxForm } from 'redux-form';

const Edit = ({ handleSubmit, pristine, submitting, loading }) => {
	return (
		<form onSubmit={handleSubmit}>
			<Field
						name="evento"
						items={['TESTE','TESTE']}
						component={SelectField}
						//handleChange={handleChange}
						id="evento"
						chosenValue={'state.evento'}
						label="Tipo de Envento"
					/>
			<DialogActions>
				<Button onClose type="submit" fullWidth variant="contained" color="primary" disabled={pristine || submitting}>
					{loading ? 'AGUARDE...' : 'ALTERAR'}
				</Button>
			</DialogActions>
		</form>
	);
};
export default reduxForm({ form: 'form' })(Edit);
