import React, { Component } from 'react';
import View from './components/view';
import Edit from './components/edit';
import EventService from '../../services/EventService';

class Event extends Component {
	constructor() {
		super();
		this.state = {
			event: {},
			error: false,
			render: false,
		};
	}

	handleUpdateEvent = async event => {
		try {
			console.log('event', event);
			event.guests = parseInt(event.guests)
			const { _id } = this.state.event;
			const { error, data } = await EventService.updateEvent(_id, event);
			if (error) throw new Error(error);
			this.setState(state => ({ ...state, event: data, render: true }));
		} catch (err) {
			console.log(`Deu ruim, ${JSON.stringify(err)}`);
		}
	};

	componentDidMount = async () => {
		const { event } = this.props;
		console.log(event);
		this.setState(state => ({ ...state, event, render: true }));
	};

	render() {
		const { event, render } = this.state;
		return (
			render && (
				<View data={event} handleUpdateEvent={this.handleUpdateEvent}/>
					
			)
		);
	}
}

export default Event;
