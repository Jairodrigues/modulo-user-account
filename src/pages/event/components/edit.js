import React from 'react';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import TextField from '../../../common/Form/textField';
import { Field, reduxForm } from 'redux-form';

const Edit = ({ handleSubmit, pristine, submitting, loading }) => {
	return (
		<form onSubmit={handleSubmit}>
			<Field name="date" component={TextField} label="Data" type="text" />
			<Field name="eventSchedule" component={TextField} label="Horário do evento" type="text" />
			<Field name="type" component={TextField} label="Cardapio" type="text" />
			<Field name="guests" component={TextField} label="Convidados" type="number" />
			<Field name="arrival" component={TextField} label="Horário de Chegada" type="text" />
			<Field name="location" component={TextField} label="Localização" type="text" />
			<Field name="amount" component={TextField} label="Valor do evento" type="text" />
			<DialogActions>
				<Button type="submit" fullWidth variant="contained" color="primary" disabled={pristine || submitting}>
					{loading ? 'AGUARDE...' : 'ADICIONAR'}
				</Button>
			</DialogActions>
		</form>
	);
};
export default reduxForm({ form: 'form' })(Edit);
