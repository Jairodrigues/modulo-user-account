import React from 'react';
import PropTypes from 'prop-types';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Card from '@material-ui/core/Card';
import { DialogModal } from '../../../common';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import AccessTime from '@material-ui/icons/AccessTime';
import Avatar from '@material-ui/core/Avatar';
import CalendarToday from '@material-ui/icons/CalendarToday';
import AddAlarm from '@material-ui/icons/AddAlarm';
import RestaurantMenu from '@material-ui/icons/RestaurantMenu';
import SupervisorAccount from '@material-ui/icons/SupervisorAccount';
import ReportProblem from '@material-ui/icons/ReportProblem';
import LocationOn from '@material-ui/icons/LocationOn';
import Payment from '@material-ui/icons/Payment'
import Edit from './edit';

const View = ({ data, handleUpdateEvent }) => {
	const { date, guests, type, location, eventSchedule, arrival, amount} = data;
	return (
		<Card style={{ marginTop: '10px' }}>
			<CardContent>
				<List>
					<Grid container spacing={24}>
						<Grid item xs={12} sm={4}>
							<ListItem>
								<Avatar>
									<CalendarToday />
								</Avatar>
								<ListItemText primary="Data do evento" secondary={date} />
							</ListItem>
							<ListItem>
								<Avatar>
									<AccessTime />
								</Avatar>
								<ListItemText primary="Horário de inicio" secondary={eventSchedule} />
							</ListItem>

							<ListItem>
								<Avatar>
									<ReportProblem />
								</Avatar>
								<ListItemText primary="Observações" secondary="" />
							</ListItem>
						</Grid>
						<Grid item xs={12} sm={4}>
							<ListItem>
								<Avatar>
									<RestaurantMenu />
								</Avatar>
								<ListItemText primary="Cardápio" secondary={type} />
							</ListItem>
							<ListItem>
								<Avatar>
									<SupervisorAccount />
								</Avatar>
								<ListItemText primary="Convidados" secondary={guests} />
							</ListItem>
							<ListItem>
								<Avatar>
									<AddAlarm />
								</Avatar>
								<ListItemText primary="Horário de Chegada" secondary={arrival} />
							</ListItem>
						</Grid>
						<Grid item xs={12} sm={4}>
							<ListItem>
								<Avatar>
									<LocationOn />
								</Avatar>
								<ListItemText primary="Localização" secondary={location} />
							</ListItem>
							<ListItem>
								<Avatar>
									<Payment />
								</Avatar>
								<ListItemText primary="Valor Total do Evento" secondary={amount} />
							</ListItem>
						</Grid>
					</Grid>
				</List>
			</CardContent>
			<CardActions style={{ padding: '20px' }}>
				<DialogModal title="Editar dados do Evento">
				<Edit onSubmit={handleUpdateEvent} initialValues={data} />
				</DialogModal>
			</CardActions>
		</Card>
	);
};

View.propTypes = {
	event: PropTypes.object.isRequired,
};

View.defaultProps = {
	event: {},
};

export default View;
