import React, { Component } from 'react';
import View from './components/view';
import CustomerServices from '../../services/CustomerServices';

class Custumers extends Component {
	constructor() {
		super();
		this.state = {
			user: {},
			render: false,
		};
	}

	getUserById = async () => {
		const { userId } = this.props;
		try {
			const { error, data } = await CustomerServices.getCustumerById(userId);
			if (error) throw new Error(error);
			this.setState(state => ({ ...state, user: data, render: true }));
		} catch (err) {
			console.log(`Deu ruim, ${JSON.stringify(err)}`);
			window.location.reload();
		}
	};

	handleUpdateCustumer = async newCustumer => {
		try {
			const { userId } = this.props;
			const { error, data } = await CustomerServices.updateCustumer(userId, newCustumer);
			if (error) throw new Error(error);
			this.setState(state => ({ ...state, user: data, render: true }));
		} catch (err) {
			console.log(`Deu ruim, ${JSON.stringify(err)}`);
		}
	};

	componentDidMount() {
		this.getUserById();
	}

	render() {
		const { user, render } = this.state;
		return (
			render && (
				<View data={user} handleUpdateCustumer={this.handleUpdateCustumer}/>
					
			)
		);
	}
}

export default Custumers;
