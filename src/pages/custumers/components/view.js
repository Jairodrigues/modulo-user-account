import React from 'react';
import PropTypes from 'prop-types';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Card from '@material-ui/core/Card';
import { DialogModal } from '../../../common';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import Fingerprint from '@material-ui/icons/Fingerprint';
import Email from '@material-ui/icons/Email';
import Phone from '@material-ui/icons/Phone';
import AccountCircle from '@material-ui/icons/AccountCircle';
import LocationOn from '@material-ui/icons/LocationOn';
import QuestionAnswer from '@material-ui/icons/QuestionAnswer';

import Edit from './form';

const View = ({ data, handleUpdateCustumer }) => {
	const { name, type, phone, email, address, cep, neighborhood, obs } = data;
	return (
		<Card style={{ marginTop: '10px' }}>
			<CardContent>
				<List>
					<Grid container spacing={24}>
						<Grid item xs={12} sm={6}>
							<ListItem>
								<Avatar>
									<AccountCircle />
								</Avatar>
								<ListItemText primary="Solicitante" secondary={name} />
							</ListItem>
							<ListItem>
								<Avatar>
									<Fingerprint />
								</Avatar>
								<ListItemText primary="Tipo de pessoa" secondary={type} />
							</ListItem>
							<ListItem>
								<Avatar>
									<Phone />
								</Avatar>
								<ListItemText primary="Telefone" secondary={phone} />
							</ListItem>
							<ListItem>
								<Avatar>
									<Email />
								</Avatar>
								<ListItemText primary="Email" secondary={email} />
							</ListItem>
						</Grid>
						<Grid item xs={12} sm={6}>
							<ListItem>
								<Avatar>
									<LocationOn />
								</Avatar>
								<ListItemText primary="Endereço" secondary={address} />
							</ListItem>
							<ListItem>
								<Avatar>
									<LocationOn />
								</Avatar>
								<ListItemText primary="Cidade" secondary={neighborhood} />
							</ListItem>
							<ListItem>
								<Avatar>
									<LocationOn />
								</Avatar>
								<ListItemText primary="CEP" secondary={cep} />
							</ListItem>
							<ListItem>
								<Avatar>
									<QuestionAnswer />
								</Avatar>
								<ListItemText primary="Observações" secondary={obs} />
							</ListItem>
						</Grid>
					</Grid>
				</List>
			</CardContent>
			<CardActions style={{ padding: '20px' }}>
				<DialogModal title="Editar dados do Cliente"><Edit onSubmit={handleUpdateCustumer} initialValues={data}/></DialogModal>
			</CardActions>
		</Card>
	);
};

View.propTypes = {
	user: PropTypes.object.isRequired,
};

View.defaultProps = {
	user: {},
};

export default View;
