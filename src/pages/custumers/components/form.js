import React from 'react';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import TextField from '../../../common/Form/textField';
import { Field, reduxForm } from 'redux-form';

const Edit = ({ handleSubmit, pristine, submitting, loading }) => {
	return (
		<form onSubmit={handleSubmit}>
			<Field name="name" component={TextField} label="Nome" type="text" />
			<Field name="email" component={TextField} label="Email" type="email" />
			<Field name="phone" component={TextField} label="Telefone" type="number" />
			<Field name="address" component={TextField} label="Endereço" type="text" />
			<Field name="neighborhood" component={TextField} label="Bairro" type="text" />
			<Field name="cep" component={TextField} label="CEP" type="number" />
			<DialogActions>
				<Button onClose type="submit" fullWidth variant="contained" color="primary" disabled={pristine || submitting}>
					{loading ? 'AGUARDE...' : 'ADICIONAR'}
				</Button>
			</DialogActions>
		</form>
	);
};
export default reduxForm({ form: 'form' })(Edit);
