import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import theme from './utils/theme';
import Router from '../src/routes';
import registerServiceWorker from './registerServiceWorker';
import { MuiThemeProvider } from '@material-ui/core';
import configureStore from './store';
import { PersistGate } from 'redux-persist/lib/integration/react';

const { store, persistor } = configureStore();

ReactDOM.render(
	<Provider store={store}>
		<PersistGate loading={null} persistor={persistor}>
			<MuiThemeProvider theme={theme}>
				<BrowserRouter>
					<Router />
				</BrowserRouter>
			</MuiThemeProvider>
		</PersistGate>
	</Provider>,
	document.getElementById('root')
);
registerServiceWorker();
