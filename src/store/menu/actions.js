import * as types from './action-types';
import MenuService from '../../services/MenuService';

export const rejectMenu = error => ({
	type: types.MENU_REJECT,
	message: error.message,
	error: true,
});

export const fulfillMenu = response => ({
	type: types.MENU_FULFILL,
	payload: response.data,
});

export const menu = () => async dispatch => {
	try {
		const response = await MenuService.getMenus();
		return dispatch(fulfillMenu(response));
	} catch (error) {
		return dispatch(rejectMenu(error));
	}
};
