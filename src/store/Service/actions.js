import ServiceHelper from '../../utils/serviceHelper';
import { URL } from '../../utils/urlJoin';
import { fulfill, reject } from '../../utils/handleData';

export const getServiceByEventId = async eventId => {
	try {
		const response = await ServiceHelper.SendGet(`${URL}/services?id=${eventId}`);
		return fulfill(response);
	} catch (error) {
		return reject(error);
	}
};

export const getServiceOptions = async () => {
	try {
		const response = await ServiceHelper.SendGet(`${URL}/optionsService`);
		return fulfill(response);
	} catch (error) {
		return reject(error);
	}
};

export const addOptionInServices = async payload => {
	try {
		const response = await ServiceHelper.SendPost(`${URL}/optionsService`, payload);
		return fulfill(response);
	} catch (error) {
		return reject(error);
	}
};
