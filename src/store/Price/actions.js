import * as types from './action-types';
import PriceService from '../../services/PriceService';

export const reject = (error, type) => ({
	type,
	message: error.message,
	error: true,
});

export const fulfill = (response, type) => ({
	type,
	payload: response.data,
});

export const pricePerBudget = data => async dispatch => {
	try {
		const response = await PriceService.getPriceBudget(data);
		return dispatch(fulfill(response, types.PRICEBUDGET_FULFILL));
	} catch (error) {
		return dispatch(reject(error, types.PRICEBUDGET_REJECT));
	}
};
