import * as types from './action-types';
import EventService from '../../services/EventService';

const reject = (error, type) => ({
	type,
	message: error.message,
	error: true,
});

const fulfill = (response, type) => ({
	type,
	payload: response.data,
});

export const add = payload => async () => {
	try {
		await EventService.newEvent(payload);
		return { message: 'Orçamento criado com sucesso', error: false };
	} catch (error) {
		return { message: 'Não foi possivel criar o orçamento', error: true };
	}
};

export const getEventById = payload => async dispatch => {
	try {
		const response = await EventService.eventById(payload);
		return dispatch(fulfill(response, types.EVENT_FULFILL));
	} catch (error) {
		return dispatch(reject(error, types.EVENT_REJECT));
	}
};
