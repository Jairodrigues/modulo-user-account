import { get, isEmpty } from 'lodash';

export const getToken = state => get(state, 'signIn.token');

export const getCurrentUserStatus = state => get(state, 'signIn.status');

export const isLoggedIn = state => (!isEmpty(getToken(state)) && getCurrentUserStatus(state) === 'Active');