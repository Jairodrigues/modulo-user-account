import * as types from './action-types';

export const initialState = {
	error: false,
	message: null,
};

export function signIn(state = initialState, action) {
	switch (action.type) {
		case types.SIGNIN_REJECT: {
			return {
				...state,
				error: action.error,
				message: action.message,
			};
		}
		case types.SIGNIN_FULFILL: {
			return {
				...state,
				error: false,
				token: action.payload,
				status: action.status,
			};
		}
		default: {
			return state;
		}
	}
}
