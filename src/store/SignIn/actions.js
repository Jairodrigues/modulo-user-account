import * as types from './action-types';
import AccountService from '../../services/AccountService';

export const rejectSignIn = error => ({
	type: types.SIGNIN_REJECT,
	message: error,
	error: true,
});

export const fulfillSignIn = response => ({
	type: types.SIGNIN_FULFILL,
	payload: response.data,
	status: 'Active',
});

export const login = data => async dispatch => {
	try {
		const response = await AccountService.signIn(data);
		localStorage.setItem('access_token', response.data.tokenData.token);
		return dispatch(fulfillSignIn(response));
	} catch (error) {
		const message = 'Usuário ou senha incorretos';
		return dispatch(rejectSignIn(message));
	}
};
