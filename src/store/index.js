import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { reducer as formReducer } from 'redux-form';
import { signIn } from './SignIn/reducer';
import { composeWithDevTools } from 'redux-devtools-extension';
import { persistReducer, persistStore } from 'redux-persist';

import storage from 'redux-persist/lib/storage';

const reducers = combineReducers({ form: formReducer, signIn });

const persistConfig = {
	key: 'root',
	storage,
	blacklist: ['form'],
};

const persistedReducer = persistReducer(persistConfig, reducers);

export default () => {
	let store = createStore(persistedReducer, composeWithDevTools(applyMiddleware(thunkMiddleware)));
	let persistor = persistStore(store);
	return { store, persistor };
};
