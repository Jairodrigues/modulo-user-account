import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

const styles = {
	root: {
		flexGrow: 1,
	},
};

class CenteredTabs extends React.Component {
	state = {
		value: 0,
	};

	handleChange = (event, value) => {
		this.setState({ value });
	};

	render() {
		const { classes, user, event, price, menu, service, team } = this.props;
		const { value } = this.state;
		return (
			<div>
				<Paper className={classes.root}>
					<Tabs
						value={this.state.value}
						onChange={this.handleChange}
						indicatorColor="primary"
						textColor="primary"
						centered
					>
						<Tab label="Cliente" />
						<Tab label="Evento" />
						<Tab label="Cardápio" />
						<Tab label="Serviço" />
						<Tab label="Parcelas" />
					</Tabs>
				</Paper>
				{value === 0 && user}
				{value === 1 && event}
				{value === 2 && menu}
				{value === 3 && service}
				{value === 4 && price}
			</div>
		);
	}
}

CenteredTabs.propTypes = {
	classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CenteredTabs);
