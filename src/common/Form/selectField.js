import React from 'react';

import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import PropTypes from 'prop-types';
import Select from '@material-ui/core/Select';

const SelectField = ({ chosenValue, handleChange, input, id , items}) => {
	return (
		<FormControl fullWidth>
			<InputLabel htmlFor="evento">Tipo do Evento</InputLabel>
			<Select value={chosenValue} onChange={handleChange} inputProps={{ name: input.name, id: id }}>
				{items.map(item =>
				<MenuItem key={item.id} value={item.id}>{item.type}</MenuItem>
				)}
			</Select>
		</FormControl>
	);
};


SelectField.propTypes = {
	label: PropTypes.string.isRequired,
	type: PropTypes.string,
};

SelectField.defaultProps = {
	type: 'text',
};

export default SelectField;
