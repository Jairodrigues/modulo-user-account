import React from 'react';

import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import PropTypes from 'prop-types';

const TextField = ({ input, type, label, isValid, meta: { touched, error } }) => {
	if (touched && error) isValid = true;
	return (
		<FormControl margin="normal" error={isValid} required fullWidth>
			<InputLabel>{label}</InputLabel>
			<Input {...input} placeholder={label} type={type} />
			<FormHelperText id="component-error-text">{isValid && <span>{error}</span>}</FormHelperText>
		</FormControl>
	);
};

TextField.propTypes = {
	label: PropTypes.string.isRequired,
	type: PropTypes.string,
	isValid: PropTypes.bool,
};

TextField.defaultProps = {
	type: 'text',
	isValid: false,
};

export default TextField;
