import React, { Fragment } from 'react';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import { useState } from 'react';
import Dialog from '@material-ui/core/Dialog';

import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

function DialogModal({ children, title, buttonTitle }) {
	const [state, setState] = useState({
		age: '',
		open: false,
	});

	function handleClickOpen() {
		setState({ ...state, open: true });
	}

	function handleClose() {
		setState({ ...state, open: false });
	}

	return (
		<Fragment>
			<Button  variant="contained" color="primary" onClick={handleClickOpen}>
				{buttonTitle}
			</Button>

			<Dialog open={state.open} onClose={handleClose} aria-labelledby="form-dialog-title">
				<DialogTitle id="form-dialog-title">{title}</DialogTitle>
				<DialogContent>{children}</DialogContent>
			</Dialog>
		</Fragment>
	);
}

DialogModal.propTypes = {
	title: PropTypes.string,
};

DialogModal.defaultProps = {
	title: 'Titulo do Dialog',
	buttonTitle: 'EDITAR',
};

export default DialogModal;
