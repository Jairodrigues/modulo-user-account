import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import withStyles from '@material-ui/core/styles/withStyles';
import DialogTitle from '@material-ui/core/DialogTitle';
import Grid from '@material-ui/core/Grid';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import PropTypes from 'prop-types';

const styles = theme => ({
	alertIconError: {
		fontSize: '80px',
		color: theme.palette.secondary.main,
		padding: '20px',
	},
	alertIconCheck: {
		fontSize: '80px',
		color: theme.palette.primary.main,
		padding: '20px',
	},
	alertTitle: {
		paddingTop: '0px',
	},
});

const Alert = ({ close, open, classes, message, error, icon, title }) => {
	if (error) {
		icon = 'cancel';
	}

	return (
		<Dialog aria-labelledby="simple-dialog-title" open={open} onClose={close}>
			<Grid container direction="column" justify="center" alignItems="center">
				<i className={`material-icons ${error ? classes.alertIconError : classes.alertIconCheck}`}>{icon}</i>
				<DialogTitle className={classes.alertTitle}>{title}</DialogTitle>
				<DialogContent>
					<DialogContentText>{message}</DialogContentText>
				</DialogContent>
			</Grid>
		</Dialog>
	);
};

Alert.propTypes = {
	classes: PropTypes.object.isRequired,
	onClose: PropTypes.func,
	selectedValue: PropTypes.string,
	colorIcon: PropTypes.string,
};

Alert.defaultProps = {
	icon: 'check_circle',
};

export default withStyles(styles)(Alert);
