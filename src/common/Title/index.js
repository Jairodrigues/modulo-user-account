import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
	Title: {
		paddingTop: '2%',
		paddingBottom: '3%',
	},
});

const Title = ({ classes, text }) => (
	<Fragment>
		<Typography className={classes.Title} component="h1" variant="h3" align="center" color="primary" gutterBottom>
			{text}
		</Typography>
	</Fragment>
);

Title.propTypes = {
	classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Title);
