import { createMuiTheme } from '@material-ui/core';

const theme = {
	palette: {
		primary: {
			main: '#006933',
		},
		secondary: {
			main: '#e53935',
		},
		disabled: '#cdcdcd',
	},

	typography: {
		fontFamily: 'Roboto',
		useNextVariants: true,
	},
};

export default createMuiTheme(theme);
