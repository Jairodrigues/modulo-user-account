export const reject = error => ({
	payload: error.message,
	error: true,
});

export const fulfill = response => ({
	payload: response.data,
	error: false,
});
