export const required = value => (value ? undefined : 'Este campo é obrigatório');

export const email = value => (
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{0,20}$/i.test(value) ?
    'Deve ser um email válido' :
    undefined
);

export const passwordValidate = value => (
    value && !/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{6,10}$/g.test(value) ?
      'Sua senha deve ter entre 6 e 10 dígitos, misturar letras maiúsculas, minúsculas e números.' :
      undefined
  );