const fs = require('fs');
const bodyParser = require('body-parser');
const jsonServer = require('json-server');
const jwt = require('jsonwebtoken');
const cors = require('cors');

const users = require('./users.json');
const menus = require('./menus.json');
const listingEvents = require('./listingEvents.json');
const events = require('./events.json');
const custumers = require('./custumers.json');
const price = require('./price.json');
const optionsMenu = require('./optionsMenu.json');

const server = jsonServer.create();
const routes = Object.assign(users, menus, listingEvents, events, custumers, price, optionsMenu);

const router = jsonServer.router(routes);

server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());
server.use(cors());

const SECRET_WORD = 'SECRET1234';
const expiresIn = '1h';

const createToken = payload => jwt.sign(payload, SECRET_WORD, { expiresIn });
const verifyToken = token =>
	new Promise((resolve, reject) => jwt.verify(token, SECRET_WORD, (err, decode) => (decode !== undefined ? resolve(decode) : reject(err))));

const userdb = JSON.parse(fs.readFileSync('./api/users.json', 'UTF-8'));
const isAuth = ({ email, password }) => userdb.users.findIndex(user => user.email === email && user.password === password) !== -1;

server.post('/auth', (req, res) => {
	const { email, password } = req.body;
	const companyId = 309;
	if (isAuth({ email, password }) === false) {
		const status = 401;
		const message = 'Incorrect email or password';
		return res.status(status).json({ error: 'Incorrect email or password' });
	}
	const access_token = createToken({ email, password, companyId });
	res.status(200).json({ access_token });
});

server.use(/^(?!\/auth).*$/, async (req, res, next) => {
	if (req.headers.authorization === undefined || req.headers.authorization.split(' ')[0] !== 'Bearer') {
		const status = 401;
		const message = 'Error in authorization format';
		res.status(status).json({ status, message });
		return;
	}
	try {
		await verifyToken(req.headers.authorization.split(' ')[1]);
		next();
	} catch (err) {
		const status = 401;
		const message = 'Error access_token is revoked';
		res.status(status).json({ status, message });
	}
});

server.use(router);

server.listen(5000, () => {
	console.log('Run API MOCK');
});
